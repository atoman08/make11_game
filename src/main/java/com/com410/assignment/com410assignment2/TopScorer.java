package com.com410.assignment.com410assignment2;
import java.io.*;
import java.util.*;
import java.util.regex.*;                           // import java kit for regex
import java.nio.file.Paths;

public class TopScorer{

    public static final Make11 make11 = new Make11();
    Scanner input = new Scanner(System.in);
    BufferedReader reader;                          // declare reader as an object with property of BufferReader
    List topScorerArray = new ArrayList();
    //set Path link to top scorer file
    String topScorerFile = Paths.get("").toAbsolutePath().toString()+"/src/main/java/com/com410/assignment/com410assignment2/top_scorer.txt";
    public int presentPlayerScore;
    public String presentPlayerName;

    public TopScorer()
    {
        this.populateTopScorer();
    }

    public void listTopScorer()
    {
        this.populateTopScorer();
        System.out.println();
        if( topScorerArray.size() < 1) {
            System.out.println("No top scorer yet");
            //make11.menu();
        }else{
            System.out.println("List of "+Integer.toString(topScorerArray.size())+" top Scorer");
            for (Object score : topScorerArray) {
                if (!score.toString().isEmpty()) {
                    System.out.println(score.toString());
                }
            }
        }

        //make11.menu();
    }

    public void populateTopScorer()
    {
        try {
            // set file reader
            reader = new BufferedReader(new FileReader(topScorerFile));
            String line = reader.readLine();        // initiate first line and store into line as string
            topScorerArray.clear();
            //loop each line from file until line is null or line is empty
            while (line != null && line.isEmpty() == false) {
                topScorerArray.add(line);
                line = reader.readLine();
            }
            reader.close();                         // close file

            this.sortTopScorer();
        }catch(IOException e){
            e.getMessage();
            System.out.println("\n===============================================================");
            System.out.println("ERROR: File could not be populated.");
            System.out.println("===============================================================\n");
            make11.menu();
        }
    }

    public void writeTopScorer()
    {
        try{

            File toDeleteTopScorer = new File(topScorerFile);                   // set top Scorer file
            toDeleteTopScorer.delete();                                      // delete previous file

            System.out.println();
            System.out.println("-> Writing new scores ...");
            File file = new File(topScorerFile);                            // set new top scorer file

            FileWriter write = new FileWriter(file, true);           // set file to be appended

            this.sortTopScorer();
            //loop through top scorer List to add each index in List into file
            for(Object score :topScorerArray){
                if(!score.toString().isEmpty()){
                    write.append(score.toString()).append("\n");
                }
            }
            System.out.println("Done");
            write.close();

            this.listTopScorer();

        }catch(IOException e){
            e.getMessage();
            System.out.println("\n===============================================================");
            System.out.println("ERROR: File could not be written.");
            System.out.println("===============================================================\n");
            make11.menu();

        }

    }

    public void setPlayerScore(int score)
    {
        this.presentPlayerScore = score;
    }

    public void setPlayerName()
    {
        System.out.println("Provide name (single name only)");
        presentPlayerName =  input.nextLine().toUpperCase().trim();

        // return if value entered is space or character inserted
        if(presentPlayerName.isEmpty() || !Pattern.matches("^[A-Za-z\\s]*$", presentPlayerName)){
            System.out.println("Invalid input");
            this.setPlayerName(); // return error
        }
    }

    public boolean setNewTopScorer()
    {
        // perform adding value when top scorer is lesser than 5
        if(topScorerArray.size() < 5)
        {
            this.setPlayerName();
            // split names and pick the first value if name entered is more than one
            String[] sp = presentPlayerName.split(" ");
            //add name
            topScorerArray.add(sp[0]+" "+Integer.toString(presentPlayerScore));

            this.writeTopScorer();
            return true;
        }

        //=================
        // replace the last value with new score

        this.sortTopScorer();       // sort list to pick the last value as the lowest

        String lastScorer = topScorerArray.get(topScorerArray.size() - 1).toString();
        String[] sp = lastScorer.split(" ");    // split  last scorer value to name and score

        // check if the value of the last scorer is greater that present score.
        // if it greater, it will display list of top scorers and go back to menu
        if(Integer.parseInt(sp[1]) >= presentPlayerScore){
            this.listTopScorer();
            return true;
        }

        this.setPlayerName();
        // split names and pick the first value if name entered is more than one
        String[] sp2 = presentPlayerName.split(" ");

        //replace the last value
        topScorerArray.set(topScorerArray.size() - 1, sp2[0]+" "+Integer.toString(presentPlayerScore));

        // write back to file
        this.writeTopScorer();
        return true;
    }


    static class topScorer
    {
        String name;
        int score;
        public topScorer(String name, int score) {
            this.name = name;
            this.score = score;
        }
    }

    public void sortTopScorer()
    {
        topScorer[] scorers = new topScorer[topScorerArray.size()];
        for (int i = 0; i < topScorerArray.size(); i++) {
            String[] parts = topScorerArray.get(i).toString().split(" ");
            String name = parts[0];
            int score = Integer.parseInt(parts[1]);
            scorers[i] = new topScorer(name,score);
        }
        // Sort the array based on scores
        Arrays.sort(scorers, Comparator.comparingInt(scorer -> scorer.score));

        topScorerArray.clear();
        // Print the sorted array
        //for (topScorer scorer : scorers) {
            //topScorerArray.add(scorer.name + " " + scorer.score);
        //}

        for (int i = scorers.length - 1; i >= 0; i--) {
            topScorer scorer = scorers[i];
            topScorerArray.add(scorer.name + " " + scorer.score);
        }
    }

    public void finalizeScore()
    {
        System.out.println();
        //Display Player final score
        System.out.println("Total score is: "+Integer.toString(presentPlayerScore));

        // call function to set new top scorer
        setNewTopScorer();


    }


}
