package com.com410.assignment.com410assignment2;

import java.io.*;
import java.util.*;
import java.nio.file.Paths;

public class Replay {


    Scanner input = new Scanner(System.in);
    public static final List replayArray = new ArrayList();
    String replayFile = Paths.get("").toAbsolutePath()+"/src/main/java/com/com410/assignment/com410assignment2/replay.txt";

    public Replay(){

    }
    public void writeTopScorer()
    {
        try{

            File toDeleteTopScorer = new File(replayFile);                   // set top Scorer file
            toDeleteTopScorer.delete();                                      // delete previous file

            System.out.println();
            System.out.println("-> Writing reply ...");
            File file = new File(replayFile);                            // set new top scorer file

            FileWriter write = new FileWriter(file, true);           // set file to be appended


            //loop through top scorer List to add each index in List into file
            for(Object replay :replayArray){
                if(!replay.toString().isEmpty()){
                    write.append(replay.toString()).append("\n");
                }
            }
            System.out.println("Done");
            write.close();

        }catch(IOException e){
            e.getMessage();
            System.out.println("\n===============================================================");
            System.out.println("ERROR: File could not be written.");
            System.out.println("===============================================================\n");

        }

    }

    public void showReplay()
    {
        System.out.println("Replay List count: "+replayArray.size());

        System.out.println("Do you want to see replay, y/n");
        String ans = input.nextLine().toUpperCase().trim();

        if(ans.equalsIgnoreCase("y")){
            System.out.println();
            System.out.println("================================================");
            System.out.println();
            for (Object o : replayArray) {
                System.out.println(o.toString());
            }
        }
    }

    public void addReplay(String replay)
    {
        replayArray.add(replay);
    }
}
