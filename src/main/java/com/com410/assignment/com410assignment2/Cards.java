package com.com410.assignment.com410assignment2;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Pattern;

public class Cards {

    Scanner input = new Scanner(System.in);
    public static final TopScorer topScorer = new TopScorer();
    public static final Replay replay = new Replay();
    public String[] spade = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
    public String[] diamond = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
    public String[] club = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
    public String[] hearts = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};

    public List usedSpade = new ArrayList();
    public List usedDiamond = new ArrayList();
    public List usedClub = new ArrayList();
    public List usedHearts = new ArrayList();
    public List cardsInHand = new ArrayList();
    public int cardLength =12;
    public String computerCard;
    private int computerCardValue;
    private final int totalGameRounds = 8;
    private String playerSelection;


    public Cards()
    {}

    public int randNumber(int max)
    {
        Random rand = new Random();
        return rand.nextInt((max) + 1);
    }
    public String getSpade()
    {
        String value = (String) Array.get(spade, randNumber(cardLength));
        if(usedSpade.contains(value)) return getSpade();
        usedSpade.add(value);
        return value;
    }

    public String getDiamond()
    {
        String value = (String) Array.get(diamond, randNumber(cardLength));
        if(usedDiamond.contains(value)) return getSpade();
        usedDiamond.add(value);
        return value;
    }

    public String getHearts()
    {
        String value = (String) Array.get(hearts, randNumber(cardLength));
        if(usedHearts.contains(value)) return getSpade();
        usedHearts.add(value);
        return value;
    }

    public String getClub()
    {
        String value = (String) Array.get(club, randNumber(cardLength));
        if(usedClub.contains(value)) return getSpade();
        usedClub.add(value);
        return value;
    }


    public String computerCard()
    {
        return switch (randNumber(3)) {
            case 1 -> getDiamond()+"-Diamonds";
            case 2 -> getHearts()+"-Hearts";
            case 3 -> getClub()+"-Clubs";
            default -> getSpade()+"-Spades";
        };
    }

    public int getCardValue(String card)
    {
        if(card.equalsIgnoreCase("ace")) return 1;
        if(card.equalsIgnoreCase("queen") || card.equalsIgnoreCase("jack")|| card.equalsIgnoreCase("king")) return 10;
        return Integer.parseInt(card);
    }

    public void setCardsInHand(int max)
    {
        for(int i = 0; i < max; i++){
            switch (randNumber(3)) {
                case 1 -> cardsInHand.add(getDiamond()+"-Diamonds");
                case 2 -> cardsInHand.add(getHearts()+"-Hearts");
                case 3 -> cardsInHand.add(getClub()+"-Clubs");
                default -> cardsInHand.add(getSpade()+"-Spades");
            };
        }
    }

    public void showCardsInHands()
    {
        StringBuilder handsCard = new StringBuilder("Player's Hand:");
        System.out.println("Please choose a card to play from your hand");
        for(int i=0; i < cardsInHand.size(); i++)
        {
            String[] parts = cardsInHand.get(i).toString().split("-");
            System.out.println(Integer.toString(i+1)+": "+parts[0]+" of "+parts[1]);
            handsCard.append(" ").append(parts[0]).append(" of ").append(parts[1]);
            if(i  < (cardsInHand.size() - 1)) handsCard.append(",");
        }

        replay.addReplay(String.valueOf(handsCard));
    }

    public void capturePlayerSelection()
    {
        System.out.println();
        System.out.println("Enter your selection");
        playerSelection = input.nextLine().toUpperCase().trim();

        if(playerSelection.isEmpty() || !Pattern.matches("^[1-5\\s]*$", playerSelection)){
            System.out.println("Invalid input");
            capturePlayerSelection(); // reset for capturing of player's selection
        }
    }


    /**
     *
     */
    public void replaceCard()
    {
        // get integer value of game the player input
        int playerInput = Integer.parseInt(playerSelection.substring(0,1));

        //Remove game played
        cardsInHand.remove(playerInput-1);
        setCardsInHand(1);   // set Card in hand

        //get Game played by the user from Cards at hand
        String[] playerGame = cardsInHand.get(playerInput-1).toString().split("-");
        StringBuilder played = new StringBuilder("Player's plays: "+playerGame[0]+ " of "+playerGame[1]);

        if(playerSelection.length() > 1){
            for(int j = 1; j < playerSelection.length(); j++){

                // get integer value of game the player input
                int playerInput2 = Integer.parseInt(playerSelection.substring(j,j+1));

                //Remove game played
                cardsInHand.remove(playerInput2-1);
                setCardsInHand(1);   // set Card in hand

                //get Game played by the user from Cards at hand
                String[] playerGame2 = cardsInHand.get(playerInput2-1).toString().split("-");
                played.append(" ").append(playerGame2[0]).append(" of ").append(playerGame2[1]);
                if(j  < (playerSelection.length() - 1)) played.append(",");
            }
        }
        replay.addReplay(String.valueOf(played));


    }


    public void startCards()
    {
        setCardsInHand(5);   // set Card in hand
        int i = 1;
        String status = "start";
        while(status.equalsIgnoreCase("start")){


            //Get Computer Card
            computerCard= computerCard();
            String[] comp = computerCard.split("-");
            computerCardValue = getCardValue(comp[0]);

            System.out.println();
            System.out.println("------------------------ ROUND "+Integer.toString(i)+" ------------------------");
            replay.addReplay("Round "+Integer.toString(i));
            System.out.println("Computer deals: "+comp[0]+" of "+comp[1]);
            replay.addReplay("Computer Plays: "+comp[0]+" of "+comp[1]);
            System.out.println("Can you make it up to 11, or produce the same suit to to continue the run?");

            System.out.println();
            showCardsInHands(); //show cards in hand

            capturePlayerSelection(); // get Player input selection
            System.out.println();

            // get integer value of game the player input
            int playerInput = Integer.parseInt(playerSelection.substring(0,1));

            //get Game played by the user from Cards at hand
            String[] playerGame = cardsInHand.get(playerInput-1).toString().split("-");
            System.out.println("You played: "+playerGame[0]+ " of "+playerGame[1]);

            //Replace the card played and change another card if value is provided.
            replaceCard();

            if((computerCardValue + getCardValue(playerGame[0])) == 11 ){
                System.out.println("You made 11!!!");
                topScorer.presentPlayerScore++;
                replay.addReplay("Point Scored");
                i++;
            } else if (comp[1].equalsIgnoreCase(playerGame[1])) {
                System.out.println("Matching suit... continue");
                i++;
            } else{
                System.out.println("Sorry, You could not make 11.");
                System.out.println("Game Over.");
                status="stop";
                topScorer.finalizeScore();
            }

            replay.addReplay(" ");

            System.out.println();
            System.out.println("Total score is: "+Integer.toString(topScorer.presentPlayerScore));



        }






    }

}
