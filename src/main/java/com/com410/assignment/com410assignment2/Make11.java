package com.com410.assignment.com410assignment2;

import java.io.*;
import java.util.*;
import java.nio.file.Paths;                         // Import java kit for Paths

public class Make11 {
    Scanner input = new Scanner(System.in);
    String topScorerFile = Paths.get("").toAbsolutePath().toString()+"/src/main/java/com/com410/assignment/com410assignment2/top_scorer.txt";

    public static final TopScorer scorer = new TopScorer();
    public static final Cards cards = new Cards();
    public static final Replay replay = new Replay();

    public static void main(String[] args) {
        Make11 trigger = new Make11();
        trigger.menu();
    }

    public void menu()
    {
        //Display Menu options
        System.out.println();
        System.out.println("Welcome to MAKE11 GAME: ");
        System.out.println("Choose an option to get started");
        System.out.println("> [a] New Game");
        System.out.println("> [b] View Highest scores");
//        System.out.println("> [c] View Highest scores");
        System.out.println("> [x] Exit the program");
        System.out.println("Thanks ");

        int value = Character.toLowerCase(input.next().charAt(0));    // get first data of input
        switch (value){
            case 'a':
                System.out.println();
                System.out.println("> Game of Cards Begin.");
                cards.startCards();
                replay.showReplay();
                this.menu();
                break;
            case 'b':
                scorer.listTopScorer();
                break;
//            case 'c':
//                this.testSetTopScorer();
//                break;
            case 'x': this.closeGame(); break;
            default:
                System.out.println("Invalid input supplied");
                this.menu();

        }
    }

    public void closeGame()
    {
        System.out.println();
        System.out.println("Game is being close, see you next time.");
        System.out.println();
        System.exit(0);
    }

    public void testSetTopScorer()
    {
        System.out.println();
        System.out.println("Enter new score");

        int score = Integer.parseInt(String.valueOf(input.nextInt()));
        scorer.setPlayerScore(score);
        scorer.setNewTopScorer();
    }


}